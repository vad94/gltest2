#include <unistd.h>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "glutils.h"
#include "glengine.h"
#include "trianglerenderer.h"

int main(int argc, char *argv[])
{
    try
    {
        GLRenderEngine::InitConfig cfg;
        cfg.color_bits = 1;
        cfg.depth_bits = 1;
        cfg.surface_type = GLRenderEngine::InitConfig::SURFACE_WINDOW;
        cfg.width = 640;
        cfg.height = 480;

        if (argc == 2 && (strcmp(argv[1], "-pbuffer") == 0))
        {
            cfg.surface_type = GLRenderEngine::InitConfig::SURFACE_PBUFFER;
            printf("Init OpengGL using pbuffer...\n");
        }

        boost::shared_ptr<GLRenderEngine> engine(new GLRenderEngine());
        engine->Init(cfg);

        boost::shared_ptr<TriangleRenderer> triangle_renderer(new TriangleRenderer());

        engine->InitRenderObject(triangle_renderer);

        engine->Draw();
        usleep(1000000);

        engine->Draw();
        usleep(1000000);

    }
    catch (MessageException e)
    {
       printf(e.GetMessage().c_str());
    }

    //event_loop(x_dpy, win, egl_dpy, egl_surf);
    return 0;
}
